#!/usr/bin/env checkio --domain=py run three-words

# https://py.checkio.org/mission/three-words/

# Let's teach the Robots to distinguish words and numbers.
#
# You are given a string with words and numbers separated by whitespaces (one space).
# The words contains only letters. You should check if the string contains 3 words in succession.
# For example, the string "start 5 one two three 7 end" contains 3 words in succession.
#
# Input: A string with words.
#
# Output: The answer as a boolean.
#
# Precondition: The input contains words and/or numbers. There are no mixed words
# (letters and digits combined).
# 0 < len(words) < 100
#
#
# END_DESC


def checkio(words: str) -> bool:
    counter = 0
    for word in words.split():
        counter = counter + 1 if word.isalpha() else 0
        if counter == 3:
            return True

    return False


if __name__ == '__main__':
    assert checkio("Hello World hello") is True, "Hello"
    assert checkio("He is 123 man")     is False, "123 man"
    assert checkio("1 2 3 4")           is False, "Digits"
    assert checkio("bla bla bla bla")   is True, "Bla Bla"
    assert checkio("Hi")                is False, "Hi"
    print("Coding complete? Click 'Check' to review your tests and earn cool rewards!")
