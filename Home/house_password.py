#!/usr/bin/env checkio --domain=py run house-password

# https://py.checkio.org/mission/house-password/

# The password will be considered strong enough if
# its length is greater than or equal to 10 symbols,
# it has at least one digit,
# as well as containing one uppercase letter and one lowercase letter in it.
# The password contains only ASCII latin letters or digits.
#
# Input: A password as a string.
#
# Output: Is the password safe or not as a boolean
# or any data type that can be converted and processed as a boolean.
# In the results you will see the converted results.
#
# Precondition:
# re.match("[a-zA-Z0-9]+", password)
# 0 < len(password) ≤ 64
#
#
# END_DESC


def checkio(data: str) -> bool:
    return len(data) >= 10 \
        and any(x.isupper() for x in data) \
        and any(x.isdigit() for x in data) \
        and any(x.islower() for x in data)


#2 - readable regex
# import re


# def checkio_2(data: str) -> bool:
#     return len(data) > 9 and \
#             all(re.search(pattern, data) for pattern in ('[A-Z]', r'\d', '[a-z]'))


if __name__ == '__main__':
    assert checkio('A1213pokl') is False, "1st example"
    assert checkio('bAse730onE4') is True, "2nd example"
    assert checkio('asasasasasasasaas') is False, "3rd example"
    assert checkio('QWERTYqwerty') is False, "4th example"
    assert checkio('123456123456') is False, "5th example"
    assert checkio('QwErTy911poqqqq') is True, "6th example"
    print("Coding complete? Click 'Check' to review your tests and earn cool rewards!")
